### Play with

- https://github.com/amcereijo/node-certification/tree/master/error-handling
- https://codeburst.io/node-js-process-errors-are-broken-193980f0a77b + https://github.com/ehmicky/log-process-errors
- https://github.com/mcollina/make-promises-safe
- https://github.com/maxgherman/node-training/tree/master/labs/exceptions
- https://github.com/rodmoreno/lfw211/tree/main/chapter-10
- https://github.com/bevry-archive/nodefailsafe


### Read the code

- https://github.com/kryz81/awesome-nodejs-learning#handling-errors
- https://github.com/ElemeFE/node-interview/blob/master/sections/en-us/error.md
- https://stackoverflow.com/questions/7310521/node-js-best-practice-exception-handling/7313005#7313005
- https://github.com/nodedocs/Exception-handling-conventions
