### Cinema

- [NodeJS EventEmitter](https://www.youtube.com/playlist?list=PLL1UEcDHVPjk4mLBqVC9PHDdb17oDE4ov)

### Read the code

- https://nodejs.org/api/events.html
- https://github.com/LeCoupa/awesome-cheatsheets/blob/master/backend/node.js#L198


### Play with

- https://github.com/JoseJPR/nodejs-certification/tree/master/src/events
- https://github.com/amcereijo/node-certification/tree/master/events
- https://github.com/rodmoreno/lfw211/tree/main/chapter-9
- [Catch all events](https://github.com/oleics/node-caevents)


### Practice

- https://github.com/Node-Study-Guide/openjs-nodejs-application-developer-study-guide/blob/master/events/index.md#exercise
