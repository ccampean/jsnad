### Cinema

* https://erickwendel.com/talk/detail/5ee6b2452c16eb4db7e7b776
* https://www.youtube.com/watch?v=a8W90jDHSho
* [NodeJS: Buffer](https://www.youtube.com/playlist?list=PLL1UEcDHVPjkAFSkrA-GOKQuWSjWYZ2mL)


### Read the code

* https://nodejs.org/api/stream.html
* https://github.com/LeCoupa/awesome-cheatsheets/blob/master/backend/node.js#L220
* https://nodejs.org/api/readline.html
* https://nodejs.org/api/buffer.html
* https://github.com/LeCoupa/awesome-cheatsheets/blob/master/backend/node.js#L538
* https://www.freecodecamp.org/news/node-js-streams-everything-you-need-to-know-c9141306be93/
* https://nodejs.org/en/knowledge/advanced/streams/how-to-use-stream-pipe
* https://komelin.com/articles/pipe-streams-node-right-way
* https://areknawo.com/node-js-file-streams-explained
* https://www.hotmess.codes/on-streams-and-promises
* https://github.com/yjhjstz/deep-into-node/blob/master/chapter6/chapter6-1.md
* https://github.com/nodedocs/What-are-streams

### Play with

* https://github.com/amcereijo/node-certification/tree/master/buffers
* https://github.com/rodmoreno/lfw211/tree/main/chapter-11
* https://github.com/amcereijo/node-certification/tree/master/streams
* https://github.com/rodmoreno/lfw211/tree/main/chapter-12
* https://github.com/Fishrock123/bob
* [Decodes a string of data which has been encoded using base-64 encoding](https://www.30secondsofcode.org/js/s/atob)
* [Creates a base-64 encoded ASCII string from a String object in which each character in the string is treated as a byte of binary data](https://www.30secondsofcode.org/js/s/btoa)
* https://www.30secondsofcode.org/js/s/is-stream
* https://www.30secondsofcode.org/js/s/is-duplex-stream
* https://www.30secondsofcode.org/js/s/is-writable-stream
* https://www.30secondsofcode.org/js/s/is-readable-stream
* [Returns an array of lines from the specified file](https://www.30secondsofcode.org/js/s/read-file-lines)
* https://github.com/lmammino/streams-workshop
