### Alumni

- [Konstantin Komelin](https://komelin.com/articles/nodejs-certification-my-experience-advice)
- [Max Gherman](https://www.max-gherman.dev/blog/2021/02/07/jsnad-certification)
- [Alexey Kalachik](https://medium.com/fively/how-to-node-js-official-certification-jsnad-c752d343e0af)
- [Ionuț Alixăndroae](https://blog.ionutalixandroae.com/posts/nodejs-jsnad-certification)
- [Krzysztof Piechowicz](https://dev.to/kryz/node-js-certification-first-impressions-21a1)
- [Juan Picado](https://twitter.com/JoseJ_PR/status/1284016426060320769)
- [Ángel Cereijo](https://t.co/YutnIJ8EEu)
- [Miguel Castillo](https://twitter.com/migramcastillo/status/1281952437377683457)


### Preparation

- https://github.com/nodejs-certified-developer/certification
- https://training.linuxfoundation.org/announcements/apply-here-for-the-node-js-certification-environment-preview-beta
- [Javascript Prerequisites](0_basics.md)
- [Control flow](1_control-flow.md)
- [Module system](2_module-system.md)
- [Events](3_events.md)
- [Buffers and streams](4_streams_buffers.md)
- [Error handling](5_error-handling.md)
- [Process/Operating System](6_process_os.md)
- [Child processes](7_child-processes.md)
- [File system](8_file-system.md)
- [Command line](9_command-line.md)
- [Package.json](10_npm.md)
- [Diagnostics (Basics, Debugging, Performance)](11_diagnostics.md)
- [Unit testing](12_testing.md)

