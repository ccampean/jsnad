### Cinema

- [Need to Node: Blacklisting Node.js Core Modules](https://vimeo.com/164607073)


### Read the code

- http://nodejs.org/api/modules.html
- https://github.com/LeCoupa/awesome-cheatsheets/blob/master/backend/node.js#L71
- https://thlorenz.com/blog/how-to-detect-if-a-nodejs-module-is-run-as-a-script
- [A proposal for static modules](https://twitter.com/threepointone/status/1461844832230514697)
- https://thomashunter.name/posts/2021-02-08-nodejs-modules-packages-and-semver
- https://github.com/kryz81/awesome-nodejs-learning#module-system
- https://github.com/mattdesl/module-best-practices
- https://leanpub.com/modulepatterns/read_full

### Play with

- https://github.com/amcereijo/node-certification/tree/master/module-system
- https://github.com/rodmoreno/lfw211/tree/main/chapter-7
- [Loads a module after removing it from the cache (if exists)](https://www.30secondsofcode.org/js/s/require-uncached)
