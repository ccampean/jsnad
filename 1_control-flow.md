### Cinema

- https://jstevenperry.wordpress.com/2018/12/07/node-js-learning-path-unit-5-the-event-loop + https://developer.ibm.com/learningpaths/get-started-nodejs/intro-to-event-loops
- [Need to Node - Tracing the Event Loop with Asyncwrap](https://vimeo.com/183883647)
- https://thomashunter.name/presentations/javascript-event-loop + https://github.com/tlhunter/presentations/tree/master/javascript-event-loop
- https://thomashunter.name/presentations/async-await-javascript + https://youtu.be/tFmk4__EHFw
- https://nebri.us/talk/berlin.js-2019-the-node.js-event-loop-not-so-single-threaded.html + https://www.slideshare.net/NodejsFoundation/nodes-event-loop-from-the-inside-out-sam-roberts-ibm
- https://gist.github.com/talohana/53ea41994adaa8e756ebd0a2c7c57b3b

### Read the source

- https://imgur.com/iXP4Igo + [Event Loop and the Big Picture — NodeJS Event Loop Part 1](https://t.co/85IfRrL8tt) + [Timers, Immediates and Process.nextTick— NodeJS Event Loop Part 2](https://t.co/wuA2LnaHr2) + [Promises, Next-Ticks, and Immediates— NodeJS Event Loop Part 3](https://t.co/tiVFVP1d1e) + [Handling IO — NodeJS Event Loop Part 4](https://t.co/ttzaKDRTU8) + [Event Loop Best Practices — NodeJS Event Loop Part 5](https://t.co/ySEtWYhNIe) + [New Changes to the Timers and Microtasks in Node v11.0.0 ( and above)](https://t.co/QQWa1fsWW7) + [JavaScript Event Loop vs Node JS Event Loop](https://t.co/pu5JULUCUg)
- https://blog.softup.co/node-js-internals-event-loop-in-action
- https://blog.risingstack.com/node-js-async-best-practices-avoiding-callback-hell-node-js-at-scale
- https://guides.codepath.com/nodejs/Control-flow
- https://thomashunter.name/posts/2017-09-03-refactoring-a-nodejs-codebase-using-async-await
- https://github.com/kryz81/awesome-nodejs-learning#event-loop

### Play with

- https://github.com/amcereijo/node-certification/tree/master/control-flow
- https://github.com/rodmoreno/lfw211/tree/main/chapter-8
- https://github.com/pgte/handson_nodejs_source_code/tree/master/chapters/flow
- [Check if the event loop is blocked](https://github.com/tj/node-blocked)



