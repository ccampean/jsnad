### Cinema

- [NodeJS: FileSystem](https://www.youtube.com/playlist?list=PLL1UEcDHVPjkGjqM4mvAb2z9meV7jWmbd)

### Read the code

- https://nodejs.org/api/fs.html
- https://github.com/LeCoupa/awesome-cheatsheets/blob/master/backend/node.js#L278
- [A 'safe' approach for 'fs.readFile()'](https://blog.yanivkessler.com/articles/a-hideous-bug/index.html)
- https://github.com/nodedocs/Managing-file-system-paths
- https://github.com/nodedocs/Introduction-to-File-System-Security
- https://github.com/nodedocs/Writing-Files


### Play with

- https://github.com/amcereijo/node-certification/tree/master/file-system
- https://github.com/JoseJPR/nodejs-certification/tree/master/src/file-system
- https://github.com/rodmoreno/lfw211/tree/main/chapter-13
