### Cinema

- https://github.com/sw-yx/cli-cheatsheet
- [The NodeJS Console API](https://www.youtube.com/playlist?list=PLL1UEcDHVPjlmwfe3OlwhZT7LUTrbMW-8)


### Read the source

- https://nodejs.org/api/cli.html
- https://github.com/LeCoupa/awesome-cheatsheets/blob/master/backend/node.js#L37
- https://github.com/qufei1993/Nodejs-Roadmap/blob/master/docs/nodejs/console.md
- https://nodejs.org/api/repl.html
- https://dev.to/nyxtom/using-the-built-in-diagnostic-report-generation-in-node-js-dl1
- https://blog.risingstack.com/mastering-the-node-js-cli-command-line-options
- https://github.com/lirantal/nodejs-cli-apps-best-practices
- https://www.javatpoint.com/nodejs-command-line-options
- [10 Useful NodeJS Command Line Flags for Everyday Development](https://t.co/Xjj793XLIZ)
- https://github.com/goldbergyoni/nodebestpractices/blob/master/sections/production/installpackageswithnpmci.md

### Play with

- https://github.com/JoseJPR/nodejs-certification/tree/master/src/console
- https://dev.to/nyxtom/bash-kung-fu-25-commands-to-master-in-the-shell-35nn
- https://github.com/sindresorhus/npm-home
- https://github.com/kevva/brightness-cli
- https://github.com/gillstrom/battery-level
- https://github.com/joakin/do-i-have-internet
- [List of projects that provide terminal user interfaces](https://github.com/rothgar/awesome-tuis)
- [Checks if the current process's arguments contain the specified flags](https://www.30secondsofcode.org/js/s/has-flags)
- https://github.com/nodedocs/Creating-a-Custom-REPL

### Question

- https://github.com/Gauthamjm007/Backend-NodeJS-Golang-Interview_QA#what-is-the-datatype-of-console
- https://github.com/Gauthamjm007/Backend-NodeJS-Golang-Interview_QA#explain-repl-in-nodejs

