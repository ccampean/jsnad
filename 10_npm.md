### Cinema

- https://thomashunter.name/presentations/node-package-management
- https://thomashunter.name/presentations/node-github-actions + https://youtu.be/Q4ZIMCce9Dw
- https://thomashunter.name/presentations/npm-ecosystem-attacks + https://github.com/tlhunter/presentations/tree/master/npm-ecosystem-attacks
- https://jstevenperry.wordpress.com/2019/01/27/node-js-learning-path-unit-8-managing-node-dependencies

### Play with

- https://github.com/sindresorhus/npm-name-cli
- https://github.com/rodmoreno/lfw211/tree/main/chapter-6

### Read the code

- https://nodejs.org/api/packages.html
- https://jbeckwith.com/2019/12/18/package-lock
- https://github.com/kryz81/awesome-nodejs-learning#npm
