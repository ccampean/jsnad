### Cinema

- https://jstevenperry.wordpress.com/2019/02/03/node-js-learning-path-unit-9-testing

### Play with

- https://github.com/rodmoreno/lfw211/tree/main/chapter-16

### Read the code

- http://nodejs.org/api/assert.html
- https://github.com/LeCoupa/awesome-cheatsheets/blob/master/backend/node.js#L492
- https://github.com/kryz81/awesome-nodejs-learning#testing
