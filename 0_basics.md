### Cinema

- https://github.com/leonardomso/33-js-concepts#9-message-queue-and-event-loop
- https://thomashunter.name/presentations/javascript-advanced-objects + https://vimeo.com/287710839 + https://thomashunter.name/posts/2018-08-16-javascript-object-property-descriptors-proxies-and-preventing-extension
- https://github.com/rjoydip/awesome-js-resources#nodejs

### Read the source

- https://blog.domenic.me/byte-sources-introduction
- https://github.com/google/node-sec-roadmap


### Play with

- https://github.com/amcereijo/node-certification/tree/master/prototype
- https://github.com/amcereijo/node-certification/tree/master/scope-chains-closures
- [A Promise-based API for setTimeout / clearTimeout](https://github.com/vitalets/await-timeout)
- https://github.com/rodmoreno/lfw211/tree/main/chapter-5
- https://github.com/workshopper/scope-chains-closures
