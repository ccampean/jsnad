### Read the code

- https://nodejs.org/api/process.html
- https://github.com/LeCoupa/awesome-cheatsheets/blob/master/backend/node.js#L102
- https://github.com/ElemeFE/node-interview/blob/master/sections/en-us/process.md
- https://github.com/JoseJPR/nodejs-certification/tree/master/src/process
- https://nodejs.org/api/os.html
- https://github.com/LeCoupa/awesome-cheatsheets/blob/master/backend/node.js#L513
- https://github.com/JoseJPR/nodejs-certification/tree/master/src/os
- https://github.com/ElemeFE/node-interview/blob/master/sections/en-us/os.md
- https://github.com/amcereijo/node-certification/tree/master/process-operating
- https://thomashunter.name/posts/2021-03-08-the-death-of-a-nodejs-process

### Play with

- https://github.com/maxgherman/node-training/tree/master/labs/process
- https://github.com/maxgherman/node-training/tree/master/labs/os
- https://github.com/rodmoreno/lfw211/tree/main/chapter-14
- [Converts a tilde path to an absolute path](https://www.30secondsofcode.org/js/s/untildify)
- [Determines if the current runtime environment is Node.js](https://www.30secondsofcode.org/js/s/is-node)
