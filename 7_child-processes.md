### Read the code

- https://nodejs.org/api/child_process.html
- https://github.com/LeCoupa/awesome-cheatsheets/blob/master/backend/node.js#L149
- https://nodestudyguide.com/child-processes
- [Connecting streams together using pipes](https://github.com/nodedocs/How-to-connect-streams)

### Play with

- https://github.com/JoseJPR/nodejs-certification/tree/master/src/child-processes
- [Process execution for humans](https://github.com/sindresorhus/execa)
- https://github.com/rodmoreno/lfw211/tree/main/chapter-15
- https://github.com/Jack-Barry/get-file-properties
