### Cinema

- [A New Way to Profile Node.js](https://www.youtube.com/watch?v=ASv8188AkVk)
- https://thomashunter.name/presentations/debug-http
- [Logging, Metrics, and Tracing with Node.js – Thomas Hunter II](https://www.youtube.com/watch?v=VFvTLfkB2d4&list=PLfIM4SvaiIyxs8QEAjNLu15skve_qJKcz) + https://thomashunter.name/presentations/node-observing-services
- https://speakerdeck.com/shuhei/profiling-node-dot-js-apps-on-production
- https://jstevenperry.wordpress.com/2019/03/03/node-js-learning-path-unit-13-debugging-and-profiling


### Read the code

- http://nodejs.org/api/util.html
- https://github.com/ElemeFE/node-interview/blob/master/sections/en-us/util.md
- https://nodejs.org/en/docs/guides/simple-profiling
- https://github.com/isaacs/node-glob/blob/master/prof.sh#L10
- https://github.com/LeCoupa/awesome-cheatsheets/blob/master/backend/node.js#L174
- https://dev.to/nyxtom/why-is-my-app-slow-profiling-node-js-with-the-built-in-profiler-4482
- https://dev.to/nyxtom/stop-using-console-log-and-use-the-node-debugger-4402
- https://gist.github.com/Ryanb58/87db80fc93edf239a36b24ea9adb73c3
- https://stackoverflow.com/questions/54338853/make-node-prof-wait-a-bit-before-recording-anything
- https://www.yld.io/blog/cpu-and-i-o-performance-diagnostics-in-node-js
- https://blog.logrocket.com/using-inbuilt-node-js-profiler
- https://github.com/kryz81/awesome-nodejs-learning#optimizing-performance
- https://github.com/kryz81/awesome-nodejs-learning#debugging
- https://stackoverflow.com/questions/1911015/how-do-i-debug-node-js-applications/16512303#16512303

### Play with

- https://github.com/nodejs/diagnostics
- https://github.com/amcereijo/node-certification/tree/master/debug
- https://github.com/JoseJPR/nodejs-certification/tree/master/src/inspector
- https://github.com/nswbmw/node-in-debugging
